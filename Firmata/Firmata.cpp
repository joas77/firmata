// Firmata.cpp : Defines the functions for the static library.
//

#include "Fimata.h"

namespace Firmata {
	Firmata::Firmata(const std::string& portName, unsigned int baudRate)
	{
		mSerialPort.setPort(portName);
		mSerialPort.setBaudRate(baudRate);
		mSerialPort.open();
	}

	Firmata::~Firmata()
	{
		exit();
	}
	void Firmata::exit()
	{
		mSerialPort.close();
	}
	void Firmata::sendSySex(uint8_t ID, const std::vector<uint8_t>& data)
	{
		std::vector<uint8_t> payload;
		payload.push_back((uint8_t)CMD::START_SYSEX);
		payload.push_back(ID);

		for (const uint8_t byte : data)
		{
			// make sure data fullfils sysex format:
			// https://github.com/firmata/protocol/blob/master/feature-registry.md
			payload.push_back(byte & 0x7F);
		}

		payload.push_back((uint8_t)CMD::END_SYSEX);
		mSerialPort.write(payload);
	}
}
