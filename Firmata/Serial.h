#pragma once
#include <string>
#include <asio.hpp>
#include <cstdint>
#include <vector>

namespace Firmata {
	class Serial
	{
	public:
		Serial();
		// Serial(const std::string& portName);
		// Serial(const std::string& portName, int baudRate);
		~Serial();

		void setBaudRate(unsigned int baudRate);
		void setPort(const std::string& portName);
		void open();
		void close();
		bool isOpen();
		size_t write(const std::string& msg);
		size_t write(const std::vector<uint8_t>& data);
		uint8_t read();
		std::vector<uint8_t> readBytes(size_t length);




	private:
		std::string mPortName;
		unsigned int mBaudRate;
		asio::io_service mIo;
		asio::serial_port mSerialPort;
	};
}

