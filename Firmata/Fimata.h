// Firmata.h
#ifndef FIRMATA_H
#define FIRMATA_H

// add headers that you want to pre-compile here
#include "framework.h"
#include <iostream>
#include <asio.hpp>
#include <cstdint>

#include "Serial.h"

namespace Firmata {
	class Firmata {
	public:
		Firmata(const std::string& portName, unsigned int baudRate=57600);
		~Firmata();
		void exit();

		void sendSySex(uint8_t ID, const std::vector<uint8_t>& data);

		static enum class CMD {
			START_SYSEX = 0xF0,
			END_SYSEX	= 0xF7
		};

	private:
		Serial mSerialPort;
		
	};
}
#endif //PCH_H
