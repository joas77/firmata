#include "Serial.h"
namespace Firmata {
	Serial::Serial():
		mBaudRate(9600),
		mSerialPort(mIo)
	{
	}

	Serial::~Serial()
	{
		if(isOpen())
			close();
	}

	void Serial::setBaudRate(unsigned int baudRate)
	{
		mBaudRate = baudRate;
		if(isOpen())
			mSerialPort.set_option(asio::serial_port::baud_rate(mBaudRate));
	}

	void Serial::setPort(const std::string& portName)
	{
		mPortName = portName;
	}

	void Serial::open()
	{
		mSerialPort.open(mPortName);
		mSerialPort.set_option(asio::serial_port::baud_rate(mBaudRate));
	}

	void Serial::close()
	{
		mSerialPort.close();
	}

	bool Serial::isOpen()
	{
		return mSerialPort.is_open();
	}

	size_t Serial::write(const std::string& msg)
	{
		return mSerialPort.write_some(asio::buffer(msg));
	}

	size_t Serial::write(const std::vector<uint8_t>& data)
	{
		return mSerialPort.write_some(asio::buffer(data));
	}

	uint8_t Serial::read()
	{
		return readBytes(1).at(0);
	}
	std::vector<uint8_t> Serial::readBytes(size_t length)
	{
		if (length == 0) length = 1;

		std::vector<uint8_t> data(length);
		asio::read(mSerialPort, asio::buffer(data, length));
		return data;
	}
}