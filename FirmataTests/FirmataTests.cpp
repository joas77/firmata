// FirmataTests.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <chrono>
#include <thread>
#include "Fimata.h"

using std::cout; 
using std::endl;
using std::cin;

std::vector<uint8_t> intToDigits(unsigned int integer);
void delayMs(unsigned int ms);

int main()
{
    std::cout << "Hello Serial Port!\n";
    /*
    Firmata::Serial ser;
    ser.setPort("COM3");
    ser.open();
    ser.setBaudRate(9600);
    
    // First reading only one byte
    std::cout << "Character readed : " << ser.read() << std::endl;
    // reading 24 bytes ...
    auto data = ser.readBytes(24);
    for (const auto& byte: data)
    {
        std::cout << (char)byte << ",";
    }
    ser.write("hello serial");
    */

    Firmata::Firmata arduino("COM3");
    cout << "Initialized Firmata protocol..."<<endl;
    cout << "press CTRL+C to exit" << endl;
    int byte = 0;
    unsigned int inputsCounter = 0;
    unsigned int outputsCounter = 0;
    while (1)
    {
        inputsCounter++;
        outputsCounter += 2;
        cout << "Simulating " << inputsCounter << " input(s)" << endl;
        cout << "Simulating " << outputsCounter << " output(s)" << endl;
        delayMs(500);
        arduino.sendSySex(0x0E, intToDigits(inputsCounter));
        delayMs(500);
        arduino.sendSySex(0x0F, intToDigits(outputsCounter));
    }
    return 0;
}

std::vector<uint8_t> intToDigits(unsigned int integer)
{
    std::vector<uint8_t> digits;

    while (integer > 0)
    {
        digits.push_back(integer % 10);
        integer = integer / 10;
    }

    return digits;
}

void delayMs(unsigned int ms)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}